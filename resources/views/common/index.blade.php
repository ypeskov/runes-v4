@extends('layouts.web')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 tabs">
            <div class="card">
                <div class="card-title m-0 pl-3">Welcome to Runes Mystery!</div>
                <div class="card-body p-3">
                    Welcome to the site where you can find answers to your questions.
                    <br>
                    <br>
                    Runic Magick and Divination can help in a situation when some decision must be taken.
                    <br>
                    <br>
                    Select the type of divination and start.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection